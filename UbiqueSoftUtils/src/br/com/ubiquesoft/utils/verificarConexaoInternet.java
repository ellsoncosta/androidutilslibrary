package br.com.ubiquesoft.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

public class verificarConexaoInternet {   

	/**
    * <h1>public {@link Boolean boolean} isOnline()</h1><br>
    * Checa se tem conex�o ou n�o com a internet.
    * @param contexto em que ser� exibido o texto
    * @return <li> retorna true se tem conex�o ou falso se n�o.
    * */
    public static boolean isOnline(Context contexto)
    {
	    ConnectivityManager cm = (ConnectivityManager) contexto.getSystemService(Context.CONNECTIVITY_SERVICE);//Pego a conectividade do contexto o qual o metodo foi chamado
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();//Crio o objeto netInfo que recebe as informacoes da NEtwork
	    System.out.println("NETWORK INFO: "+netInfo.getSubtypeName());
    
	    //Se o objeto for nulo ou nao tem conectividade retorna false
	    if ((netInfo != null) && (netInfo.isConnectedOrConnecting()) && (netInfo.isAvailable()))
	    {
	    	//Toast.makeText(contexto, R.string.conectado__internet, Toast.LENGTH_LONG).show();
	    	return true;
	    }
	    else
	    {
	    	//Toast.makeText(contexto, R.string.nao_conexao_internet, Toast.LENGTH_LONG).show();
	    	return false;
	    }
    }
    
/*    public static boolean internetConnection(Context context) 
    {   	
    	   ConnectivityManager connMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		   NetworkInfo info=connMgr.getActiveNetworkInfo();
		   boolean retorno = (info!=null && info.isConnected());
		   
		   if(retorno)
		   {
			   Toast.makeText(context, R.string.conectado__internet, Toast.LENGTH_LONG).show();
				return true;
		   }
		   else
		   {
			   Toast.makeText(context, R.string.nao_conexao_internet, Toast.LENGTH_LONG).show();
			   return false;
		   }
	}*/
    
    public static boolean isConectado(Context context) 
    { 
        boolean lblnRet = false;
        try
        {
            ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE); 
            if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) 
            { 
            	//Toast.makeText(context, R.string.conectado__internet, Toast.LENGTH_LONG).show();
                lblnRet = true; 
            } 
            else 
            { 
//            	Toast.makeText(context, R.string.nao_conexao_internet, Toast.LENGTH_LONG).show();
                lblnRet = false; 
            }
        }
        catch (Exception e) 
        {
//        	Toast.makeText(context, R.string.ocorreu_seguinte_erro + e.toString(), Toast.LENGTH_LONG).show();
        }
        return lblnRet;
    }
    
   

    /*public static boolean Conectado(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager)
            context.getSystemService(Context.CONNECTIVITY_SERVICE);
            
            if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected()) {
                    Toast.makeText(context,"Status de conex�o 3G: "+cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected(), Toast.LENGTH_LONG).show();
                    return true;
            } else if(cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected()){
                    Toast.makeText(context,"Status de conex�o Wifi: "+cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected(), Toast.LENGTH_LONG).show();
                    return true;
            } else {
                    //Toast.makeText(context, "N�o h� conex�o com a internet.", Toast.LENGTH_LONG).show();
                    return false;
            }
        } catch (Exception e) {
                //Log.e("Verifica conexao".toString(),e.getMessage());
                Toast.makeText(context, "Ocorreu o seguinte erro " + e.getMessage(), Toast.LENGTH_LONG).show();
                return false;
        }
    }*/
}
