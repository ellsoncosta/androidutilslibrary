package br.com.ubiquesoft.utils;

import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

import android.content.Context;
import android.util.Log;

public class WebClientPost 
{
	private String url;
	private Context context;
	
	public WebClientPost(String url, Context context)
	{
		this.url = url;
		this.context = context;
	}
	
	/**
	 * @param json
	 * @return
	 * 0 para tipo do retorno; ex: autorizado 200, n�o autorizado 400, 401
	 * 1 para retorno do json
	 */
	public List<String> post(String json)
	{
		//String jsonResposta = null;
		
		List<String> jsonResposta = null;
		
		try
		{
			int timeoutConnection = 3000;
			int timeoutSocket = 5000;
			
			HttpParams httpParams = new BasicHttpParams(); 
			HttpConnectionParams.setConnectionTimeout(httpParams, timeoutConnection);
			HttpConnectionParams.setSoTimeout(httpParams, timeoutSocket); 
			HttpClient httpClient = new DefaultHttpClient(httpParams);            
			
			
			HttpPost post = new HttpPost(url);
			post.setEntity(new StringEntity(json));
			
			post.setHeader("Accept", "application/json");
			post.setHeader("Content-type", "application/json");
			
			HttpResponse response = httpClient.execute(post);
			
			int status = response.getStatusLine().getStatusCode();
					
			//Toast.makeText(context, R.string.autoirizado, Toast.LENGTH_SHORT).show();					
			return jsonResposta = Arrays.asList(Integer.toString(status), EntityUtils.toString(response.getEntity()));
			
		} catch (Exception e) 
		{
			Log.e(getClass().toString(), context.getResources().getText(R.string.erro).toString() + e.getMessage());			
			return jsonResposta;// = Arrays.asList(Integer.toString(status));
		}
	}
}
