package br.com.ubiquesoft.utils;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.util.Log;

public class WebClientGet 
{
	private final String url;
	private final Context context;
	private String auth_token;
	
	public WebClientGet(String url, Context context, String auth_token)
	{
		this.url = url;
		this.context = context;
		this.auth_token = auth_token;
	}
	
	/**
	 * @param json
	 * @return
	 * 0 para tipo do retorno; ex: autorizado 200, n�o autorizado 400, 401
	 * 1 para retorno do json
	 */	
	public List<String> getRESTFileContent() 
	{		
		List<String> ret = null;
		int status = 0;
	
		HttpClient httpclient = new DefaultHttpClient();
		
		HttpGet httpget = new HttpGet(url);
		httpget.setHeader("Accept", "application/json");
		httpget.setHeader("Content-type", "application/json");
		httpget.setHeader("auth_token", auth_token);
		
		inputStream inputStream = new inputStream();

		try {
			HttpResponse response = httpclient.execute(httpget);
			
			status = response.getStatusLine().getStatusCode();

			if (status == 200) 
			{
				HttpEntity entity = response.getEntity();

				if (entity != null) {
					InputStream instream = entity.getContent();
					String result = new String(inputStream.getBytes(instream));

					instream.close();
					return ret = Arrays.asList(Integer.toString(status), result);
				}
			}
			
		} catch (Exception e) 
		{
			Log.e(getClass().toString(), context.getResources().getText(R.string.erro).toString() + e.getMessage());			
			return ret;
		}
		
		return ret = Arrays.asList(Integer.toString(status));
	}
}
