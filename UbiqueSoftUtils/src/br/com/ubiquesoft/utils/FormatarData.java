package br.com.ubiquesoft.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class FormatarData  
{
//	/** 
//	    * Converte uma String para um objeto Date. Caso a String seja vazia ou nula,  
//	    * retorna null - para facilitar em casos onde formulários podem ter campos 
//	    * de datas vazios. 
//	    * @param data String no formato dd/MM/yyyy a ser formatada 
//	    * @return Date Objeto Date ou null caso receba uma String vazia ou nula 
//	    * @throws Exception Caso a String esteja no formato errado 
//	    */ 
//	public static Date ConvertStringData(String data) throws Exception {   
//	       if (data == null || data.equals("") || data.equals("null") || data.equals(null))
//	       {
//	    	   return null;
//	       }
//	       
//	       String dataTem = data.substring(0, 4);
//	       
//	       String stringFormatoData;
//	       
//	       if (dataTem.contains("/") || dataTem.contains("-"))
//	       {
//	           if (data.contains("/")) 
//	           {
//	        	  stringFormatoData = "dd/MM/yyyy HH:mm:ss";
//	           }
//	           else
//	           {
//	        	   stringFormatoData = "dd-MM-yyyy HH:mm:ss";
//	           }
//	       }
//	       else
//	       {
//	    	   if(data.length() > 10)
//	    	   {
//	    		   if(data.contains("Z"))
//	    		   {
//	    			   stringFormatoData = "yyyy-MM-dd'T'HH:mm:ss'Z'";
//	    		   }
//	    		   else
//	    		   {
//	    			   stringFormatoData = "yyyy-MM-dd HH:mm:ss";
//	    		   }
//	    	   }
//	    	   else
//	    	   {
//	    		   stringFormatoData = "yyyy-MM-dd";
//	    	   }
//	       }
//	         
//	       Date date = null;  
//	       try {  
//	           DateFormat formatter = new SimpleDateFormat(stringFormatoData);  
//	           date = formatter.parse(data);
//	           //date = new java.sql.Date(((java.util.Date)formatter.parse(data)).getTime());           
//	       } catch (ParseException e) {              
//	           throw e;  
//	       }  
//	       return date;  
//	   }
//
//		/**
//		 * Conver formato data para sql.Date
//		 * @param Recebe um formato data e converte para sql.Date
//		 * @return Exception Caso a String esteja no formato errado 
//		 * @throws Exception 
//		 */
//		public static java.sql.Date ConvertStringData(Date data) throws Exception {		
//			if(data == null || data.equals(""))
//			{ 
//				return null; 
//			}
//			
//			java.sql.Date date = null;
//			
//			try 
//			{			
//				date = new java.sql.Date(data.getTime());
//				
//			} 
//			catch (Exception e) {
//				throw e;
//			}
//			return date;
//		}  
//		
//		
//		/**
//		 * Convert data em string
//		 * @param data
//		 * @return
//		 * @throws Exception
//		 */
//		public static String ConvertDataString(Date data) 
//		{   
//	       if (data == null || data.equals(""))
//	       {
//	    	   return null;
//	       }
//	         
//	       String date = null;  
//	       try {  
//	    	   
//	    	   SimpleDateFormat simpleFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
//	           date = simpleFormat.format(data);
//	           
//	       } catch (ParseException e) {              
//	    	   Log.e("classe: ConvertDataString", e.toString());
//	       }  
//	       return date;  
//	   }
//
//		
//		public static Date convertStringData(String data, String stringFormatoData) throws Exception
//		{   
//		   if (data == null || data.equals("") || data.equals("null") || data.equals(null))
//		   {
//			   return null;
//		   }
//		   
//		   Date date = null;  
//		   try 
//		   {  
//		       DateFormat formatter = new SimpleDateFormat(stringFormatoData);  
//		       date = (java.util.Date) formatter.parse(data);       
//	       } 
//		   catch (ParseException e) 
//		   {              
//			   Log.e("classe: convertStringData", e.toString());
//		   }  
//		   
//	       return date;  
//		 }
		
		/**
		 * @return retorna uma data no formato dd-MM-yyyy HH:mm:ss
		 */
		public static String NowToStringData()
		{
			SimpleDateFormat simpleFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
			
			return simpleFormat.format(new Date( System.currentTimeMillis()));
		}
		
		
		public static Date NowToData()
		{
			 Date date = null; 
			
			try {
				
				SimpleDateFormat simpleFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
				String data = simpleFormat.format(new Date( System.currentTimeMillis()));
				date = simpleFormat.parse(data);
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			return date;
		}
		
		public static Date ToDate(String data)
		{
			Date date = null; 
			
			try 
			{
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");     
				date = dateFormat.parse(data);
				
			} catch (Exception e) 
			{
				return null;
			}
			
			return date;
		}
		
		public static Date ToDate(Date data)
		{
			try 
			{
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");     
				return dateFormat.parse(dateFormat.format(data));
				
			} catch (Exception e) 
			{
				return null;
			}
		}
		
		
		public static String ToString(Date data)
		{
			try 
			{
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");     
				return dateFormat.format(data);
				
			} catch (Exception e) 
			{
				return null;
			}
		}
		
		
		public static String ToDate(String data, String formato)
		{		
			Date date = null; 
			
			try 
			{
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");   
				date = dateFormat.parse(data);
				
				SimpleDateFormat dateFormato = new SimpleDateFormat(formato);
				
				return dateFormato.format(date).toString();
				
			} catch (Exception e) 
			{
				return null;
			}
		}
		
		public static String ToDate(Date data, String formato)
		{
			String ret = null;
			
			try 
			{
				if (data != null) 
				{
					SimpleDateFormat dateFormato = new SimpleDateFormat(formato);
					
					ret = dateFormato.format(data).toString();
				}
				
			} catch (Exception e) 
			{
				return null;
			}
			
			return ret;
		}
		
		public static long ToDateLong(String data, String formato)
        {
            long ret = 0;
            
            try 
            {
                if (data != null) 
                {
                        SimpleDateFormat dateFormat = new SimpleDateFormat(formato);                                    
                        ret = dateFormat.parse(data).getTime();
                }
                    
            } catch (Exception e) 
            {
                    return ret;
            }
            
            return ret;
        }
		
		public static Date ToDateLong(Long data, String formato)
        {
			Date date = null; 
            
            try 
            {
                if (data != null) 
                {
                        SimpleDateFormat dateFormat = new SimpleDateFormat(formato);                                    
                        date = dateFormat.parse(dateFormat.format(data));
                }
                    
            } catch (Exception e) 
            {
                    return date;
            }
            
            return date;
        }
		
}
