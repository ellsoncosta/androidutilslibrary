package br.com.ubiquesoft.utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class FormatarMoeda 
{
	public static String Valor(Double valor)
	{
			try
			{
				if (valor != null) 
				{
					DecimalFormat formatoDois = new DecimalFormat("##,###,###,##0.00", new DecimalFormatSymbols (new Locale ("pt", "BR")));
					formatoDois.setMinimumFractionDigits(2);   
					formatoDois.setParseBigDecimal (true);  
					  
					return formatoDois.format(valor).toString();
				}
			}
			catch (Exception e) 
			{
				return "0";
			}
		
		return "0";		
	}
	
	public static double round(double valor, int casas) 
	{  
        double p = Math.pow(10, casas);  
        return Math.round(valor * p) / p;  
	}
	
	public static double trunc(double valor, int casas) 
	{  
        double p = Math.pow(10, casas);  
        return Math.floor(((valor+0.00000001) * p)) / p;  
	} 
}
