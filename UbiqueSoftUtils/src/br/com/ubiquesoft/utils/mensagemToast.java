package br.com.ubiquesoft.utils;

import android.content.Context;
import android.widget.Toast;

public class mensagemToast 
{
	 /**
     * Mensagem curta
     * @param context
     * @param msg
     */
    public static void Curta(Context context, String msg) 
	{
	    Toast.makeText (context, msg, Toast.LENGTH_SHORT).show();
	}
	    
    /**
     * Mensagem Longa
     * @param context
     * @param msg
     */
    public static void Longa(Context context, String msg) 
	{
	    Toast.makeText (context, msg, Toast.LENGTH_LONG).show();
	}
	    
	    
	    public static void MsgSucesso(Context context) 
	{
	    Toast.makeText (context, br.com.ubiquesoft.utils.R.string.operacao_realizada_sucesso, Toast.LENGTH_SHORT).show();
	}
	    
	    public static void MsgExcluir(Context context) 
	{
	    Toast.makeText (context, br.com.ubiquesoft.utils.R.string.registro_excluiro_sucesso, Toast.LENGTH_SHORT).show();
	}
	    
	    public static void MsgErroExcluir(Context context) 
	{
	    Toast.makeText (context, br.com.ubiquesoft.utils.R.string.errp_excluir_registro, Toast.LENGTH_SHORT).show();
	}

}
