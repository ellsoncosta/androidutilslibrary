package br.com.ubiquesoft.utils;

import java.io.File;

import android.content.Context;
import android.util.Log;

public class Diretorio 
{
	public static boolean verificaDiretorioExiste(Context context, String path)
	{
		//context.getDir("testeimg", Context.MODE_WORLD_READABLE).mkdirs();
		//imagens/noticia
		//imagens/solucao
		
		String dr = context.getFilesDir().getAbsolutePath();
		
		boolean ret = false;
		File file = new File(dr, path);
		
		if (!file.exists()) 
		{
			ret = file.mkdirs();
			
			//ret = context.getDir(file.getPath(), Context.MODE_WORLD_READABLE).mkdirs();
		}
		
		return ret;
	}
	
	public static boolean verificaArquivoExiste(String path)
	{	
		File file = new File(path);
		
		if (file.exists()) 
		{
			return true;
		}
		
		return false;
	}
	
	public static String pathCaminhoArquivo(Context context, String caminho)
	{
		try 
		{
			
			boolean bo = verificaDiretorioExiste(context, caminho);
			
			if (bo)
			{
				Log.d("teste", "true");
			}
			
			String pathImagem = context.getFileStreamPath("").toString();
			
			return new StringBuilder().append(pathImagem).append("/").append(caminho).append("/").toString();
			
		} catch (Exception e) 
		{
			return null;
		}
	}
}
