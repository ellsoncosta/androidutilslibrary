package br.com.ubiquesoft.utils;

import java.util.List;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.util.Log;

public class Twitter 
{
	public void Twittar(Activity context)
	{
		try{
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_TEXT, "this is a tweet");
            intent.setType("text/plain");
            final PackageManager pm = context.getPackageManager();
            final List<?> activityList = pm.queryIntentActivities(intent, 0);
            int len =  activityList.size();
            for (int i = 0; i < len; i++) {
                final ResolveInfo app = (ResolveInfo) activityList.get(i);
                if ("com.twitter.android.PostActivity".equals(app.activityInfo.name)) {
                    final ActivityInfo activity=app.activityInfo;
                    final ComponentName name=new ComponentName(activity.applicationInfo.packageName, activity.name);
                    intent.addCategory(Intent.CATEGORY_LAUNCHER);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
                    intent.setComponent(name);
                    context.startActivity(intent);
                    break;
                }
            }
      }
        catch(final ActivityNotFoundException e) {
            Log.i("twitter", "no twitter native",e );
        }
	}
}
