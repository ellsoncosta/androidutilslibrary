package br.com.ubiquesoft.utils;

public class FormatarBoolean 
{
	public static String getBoolean(boolean valor)
	{
		if (valor) 
		{
			return "true";
		}
		else
		{
			return "false";
		}
	}
	
	public static boolean getBoolean(String valor)
	{
		if (valor.equals("true")) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static boolean getBooleanRetBoolean(int valor)
	{
		if (valor == 1) 
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public static String getBooleanRetString(int valor)
	{
		if (valor == 1) 
		{
			return "true";
		}
		else
		{
			return "false";
		}
	}
}
