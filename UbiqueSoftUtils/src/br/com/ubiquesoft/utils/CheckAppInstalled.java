package br.com.ubiquesoft.utils;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

public class CheckAppInstalled 
{
	public static String GOOGLE_MAPS = "com.google.android.apps.maps";
	public static String FACEBOOK = "com.facebook.katana";
	public static String TWITTER = "com.twitter.android";
	public static String YOU_TUBE = "com.google.android.youtube";
	
	
	public static boolean AppInfor(Activity activity, String app)
	{
		try
		{
			ApplicationInfo info = activity.getPackageManager().getApplicationInfo(app, 0 );
		    return true;
		} catch( PackageManager.NameNotFoundException e )
		{
		    return false;
		}
	}
}
