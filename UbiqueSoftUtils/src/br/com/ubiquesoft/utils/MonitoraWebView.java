package br.com.ubiquesoft.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

public class MonitoraWebView 
{
	public void monitoraWebView(WebView webView, final int id, final Activity activity)
	{
		webView.setWebViewClient(new WebViewClient()
		{
			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) 
			{
				super.onPageStarted(view, url, favicon);
			}

			@Override
			public void onPageFinished(WebView view, String url) 
			{
				super.onPageFinished(view, url);
				
				ProgressBar progressBar = (ProgressBar) activity.findViewById(id);
				progressBar.setVisibility(View.INVISIBLE);
			}
		
		});
	}
}
