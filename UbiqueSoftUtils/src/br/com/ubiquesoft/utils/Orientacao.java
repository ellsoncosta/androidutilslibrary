package br.com.ubiquesoft.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;

public class Orientacao 
{
	// Verifica qual a orienta��o da tela
	public static int getOrientacao(Context context) {
		int orientacao = context.getResources().getConfiguration().orientation;
		return orientacao;
	}

	// Verifica se a orienta��o da tela est� na vertical
	public static boolean isVertical(Context context) {
		int orientacao = context.getResources().getConfiguration().orientation;
		boolean vertical = orientacao == Configuration.ORIENTATION_PORTRAIT;
		return vertical;
	}

	// Verifica se a orienta��o da tela est� na horizontal
	public static boolean isHorizontal(Context context) {
		int orientacao = context.getResources().getConfiguration().orientation;
		boolean horizontal = orientacao == Configuration.ORIENTATION_LANDSCAPE;
		return horizontal;
	}

	// Solicita para executar a activity na vertical
	public static void setOrientacaoVertical(Activity context) {
		context.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	// Solicita para executar a activity na horizontal
	public static void setOrientacaoHorizontal(Activity context) {
		context.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
	}
	
	public static boolean isHorizontalTablet(Context context)
	{	
		return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE
				& context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE;
	}
	
	public static void travaOrientacao(Activity activity)
	{
		int currentOrientation = activity.getResources().getConfiguration().orientation;

		if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) 
		{
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		} 
		else 
		{
			activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		}
	}
	
	public static void destravarOrientacao(Activity activity)
	{
		activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
	}

}
