package br.com.ubiquesoft.utils;

import android.app.AlertDialog;
import android.content.Context;



public class mensagensAlerta {
	public static void informacao(Context context, String mensagem){
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		//TITULO DA MENSAGEM
		alertDialog.setTitle("Informação");
		//MENSAGEM
		alertDialog.setMessage(mensagem);
		//ICONE DA MENSAGEM
		//alertDialog.setIcon(drawable.infor);
		
		alertDialog.show();
		
	}
	
	public static void erro(Context context, String mensagem){
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		//TITULO DA MENSAGEM
		alertDialog.setTitle("Ocorreu um erro.");
		//MENSAGEM
		alertDialog.setMessage(mensagem);
		//ICONE DA MENSAGEM
		//alertDialog.setIcon(drawable.infor_red);
		
		alertDialog.show();
		
	}
	
	public static void sucesso(Context context, String mensagem){
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		//TITULO DA MENSAGEM
		alertDialog.setTitle("Sucesso.");
		//MENSAGEM
		alertDialog.setMessage(mensagem);
		//ICONE DA MENSAGEM
		//alertDialog.setIcon(drawable.sucesso_verde);
		
		alertDialog.show();
		
	}
	
	public static void salvar(Context context, String mensagem){
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		//TITULO DA MENSAGEM
		alertDialog.setTitle("Salvar dados?");
		//MENSAGEM
		alertDialog.setMessage(mensagem);
		//ICONE DA MENSAGEM
		//alertDialog.setIcon(drawable.salve_red);
		
		alertDialog.show();
		
	}
}
