package br.com.ubiquesoft.utils;

public class FormatarString 
{
	
	/**
	 * Remove o texto e deixa com o tamanho definido, e acresenta retic�ncia '...'
	 * @param valor
	 * @param posicao
	 * @return
	 */
	public static String RemoverApartir(String valor, int posicao) 
	{
	  StringBuffer stringBuffer = new StringBuffer(valor);
	  stringBuffer.delete(posicao, valor.length());
	  return String.format("%s...", stringBuffer.toString());
	}
	
	public static String RemoverApartirCaracter(String valor, String caracter) 
	{
		int posicao = 0;
		
		for(int i = 0;i<valor.length();i++)
		{  
			if (valor.substring(i,i+1).equals(caracter))
			{  
			   posicao = i; 
			}
		}
		
		StringBuffer stringBuffer = new StringBuffer(valor);
		stringBuffer.delete(posicao, valor.length());
		return stringBuffer.toString();
	}

	 /**
     * Remove espa�os no come�o, no fim da frase e substitui espa�os duplos no meio da fase por espa�o simples.
     * @param  Texto a ser modificado
     * @return Texto modificado
     */
    public static String RemoveExcessoEspaco(String valor)
    {  	
        while (valor.contains("  ")){
        	valor = valor.replace("  ", " ");
        }

        return valor.trim();
    }
    
   
    /**
     * Remove todos os espa�os do texto.
     * @param Texto a ser alterado
     * @return Texto alterado
     */
    public static String RemoveTodoEspaco(String Texto)
    {        
        return Texto.replaceAll(" ", "");
    }
    
    
    public static String FormatarTamanho(String Texto, int valor, Boolean pontilhado)
    {    	
    	if (Texto.length() > valor) 
    	{
    		Texto = Texto.substring(0, valor);
			
			if (pontilhado) 
			{
				Texto += "...";
			}
		}
    	
    	return Texto;
    }
    
    
    /**
     * M�todo que valida um componente do tipo TextBox se o mesmo possui ou não caracteres especiais como: {,},',",%,|,#,[,]
     * @param valor a ser alterado
     * @return valor aulterado
     */
    public static String RemoverCaracteresEspeciais(String valor)
    {
        String[] verifica = { "{", "}", "'", "\"", "%", "|", "#", "[", "]", "<", ">", ".","-", "/", ",","@","$", "&","?","^","~"}; //string aspasDuplas = "\""; // aspas duplas

        for (int i = 0; i < verifica.length; i++)
        {
            if (valor.contains(verifica[i]))
            {
                valor = valor.replace(verifica[i], "");
            }
        }
        return valor;
    }
    
    
    public static boolean VerificarCarateresEspeciaisSMS(String valor)
    {
    	String[] verifica = { "ç","é","ę","č","ë","í","ď","í","ě","î","ó","ň","ô","ö","ú","ů","ű","ü","ń","ä","ĺ","á","ŕ","â","ă","ć", "Ç","É","Ę","Č","Ë","Í","Ď","Í","Ě","Î","Ó","Ň","Ô","Ö","Ú","Ů","Ű","Ü","Ń","Ä","Ĺ","Á","Ŕ","Â","Ă","Ć"};

        for (int i = 0; i < verifica.length; i++)
        {
            if (valor.contains(verifica[i]))
            {
                return true;
            }
        }
        return false;
    }
}
