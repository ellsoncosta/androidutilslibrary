package br.com.ubiquesoft.utils;

public class FormatarOrdinal 
{
	public static String Valor(int valor)
	{
		if (valor > 0) 
		{
			return new StringBuilder().append(Integer.toString(valor)).append("�").toString();
		}
		
		return null;
	}
}
