package br.com.ubiquesoft.alerta;

import android.app.Activity;
import android.view.Gravity;

public class ToastAlerta 
{

	/**
	 * @param activity
	 * @param mensagem
	 * @param 1: TOP, 2: CENTER, 3: BOTTOM.
	 */
	public static void showToastInfor(Activity activity, int mensagem, int posicao)
	{
		ToastAlertaMensagem alertaMensagem = ToastAlertaMensagem.makeText(activity, activity.getString(mensagem), ToastAlertaMensagem.STYLE_INFO);
    	alertaMensagem.setDuration(ToastAlertaMensagem.LENGTH_LONG);
    	
		switch (posicao) {
		case 1:
			alertaMensagem.setLayoutGravity(Gravity.TOP);
			break;
		case 2:
			alertaMensagem.setLayoutGravity(Gravity.CENTER);
			break;
		case 3:
			alertaMensagem.setLayoutGravity(Gravity.BOTTOM);
			break;
		default:
			break;
		}
    	
    	alertaMensagem.show();
	}
	
	
	/**
	 * @param activity
	 * @param mensagem
	 * @param 1: TOP, 2: CENTER, 3: BOTTOM.
	 */
	public static void showToastInfor(Activity activity, String mensagem, int posicao)
	{
		ToastAlertaMensagem alertaMensagem = ToastAlertaMensagem.makeText(activity, mensagem, ToastAlertaMensagem.STYLE_INFO);
    	alertaMensagem.setDuration(ToastAlertaMensagem.LENGTH_LONG);
    	
    	switch (posicao) {
		case 1:
			alertaMensagem.setLayoutGravity(Gravity.TOP);
			break;
		case 2:
			alertaMensagem.setLayoutGravity(Gravity.CENTER);
			break;
		case 3:
			alertaMensagem.setLayoutGravity(Gravity.BOTTOM);
			break;
		default:
			break;
		}
    	
    	alertaMensagem.show();
	}
	
	/**
	 * @param activity
	 * @param mensagem
	 * @param 1: TOP, 2: CENTER, 3: BOTTOM.
	 */
	public static void showToastAlert(Activity activity, String mensagem, int posicao)
	{
		ToastAlertaMensagem alertaMensagem = ToastAlertaMensagem.makeText(activity, mensagem, ToastAlertaMensagem.STYLE_ALERT);
    	alertaMensagem.setDuration(ToastAlertaMensagem.LENGTH_LONG);
    	
    	switch (posicao) {
		case 1:
			alertaMensagem.setLayoutGravity(Gravity.TOP);
			break;
		case 2:
			alertaMensagem.setLayoutGravity(Gravity.CENTER);
			break;
		case 3:
			alertaMensagem.setLayoutGravity(Gravity.BOTTOM);
			break;
		default:
			break;
		}
    	
    	alertaMensagem.show();
	}
	
	/**
	 * @param activity
	 * @param mensagem
	 * @param 1: TOP, 2: CENTER, 3: BOTTOM.
	 */
	public static void showToastSucesso(Activity activity, int posicao)
	{
		ToastAlertaMensagem alertaMensagem = ToastAlertaMensagem.makeText(activity, br.com.ubiquesoft.utils.R.string.operacao_realizada_sucesso, ToastAlertaMensagem.STYLE_INFO);
    	alertaMensagem.setDuration(ToastAlertaMensagem.LENGTH_LONG);
    	
    	switch (posicao) {
		case 1:
			alertaMensagem.setLayoutGravity(Gravity.TOP);
			break;
		case 2:
			alertaMensagem.setLayoutGravity(Gravity.CENTER);
			break;
		case 3:
			alertaMensagem.setLayoutGravity(Gravity.BOTTOM);
			break;
		default:
			break;
		}
    	
    	alertaMensagem.show();
	}
	
	/**
	 * @param activity
	 * @param mensagem
	 * @param 1: TOP, 2: CENTER, 3: BOTTOM.
	 */
	public static void showToastExcluidoSucesso(Activity activity, int posicao)
	{
		ToastAlertaMensagem alertaMensagem = ToastAlertaMensagem.makeText(activity, br.com.ubiquesoft.utils.R.string.registro_excluiro_sucesso, ToastAlertaMensagem.STYLE_INFO);
    	alertaMensagem.setDuration(ToastAlertaMensagem.LENGTH_LONG);
    	
    	switch (posicao) {
		case 1:
			alertaMensagem.setLayoutGravity(Gravity.TOP);
			break;
		case 2:
			alertaMensagem.setLayoutGravity(Gravity.CENTER);
			break;
		case 3:
			alertaMensagem.setLayoutGravity(Gravity.BOTTOM);
			break;
		default:
			break;
		}
    	
    	alertaMensagem.show();
	}
	
	/**
	 * @param activity
	 * @param mensagem
	 * @param 1: TOP, 2: CENTER, 3: BOTTOM.
	 */
	public static void showToastErroExcluir(Activity activity, int posicao)
	{
		ToastAlertaMensagem alertaMensagem = ToastAlertaMensagem.makeText(activity, br.com.ubiquesoft.utils.R.string.errp_excluir_registro, ToastAlertaMensagem.STYLE_INFO);
    	alertaMensagem.setDuration(ToastAlertaMensagem.LENGTH_LONG);
    	
    	switch (posicao) {
		case 1:
			alertaMensagem.setLayoutGravity(Gravity.TOP);
			break;
		case 2:
			alertaMensagem.setLayoutGravity(Gravity.CENTER);
			break;
		case 3:
			alertaMensagem.setLayoutGravity(Gravity.BOTTOM);
			break;
		default:
			break;
		}
    	
    	alertaMensagem.show();
	}
	
	
	
	
	
	
	
	/**
	 * @param activity
	 * @param mensagem
	 * @param 1: TOP, 2: CENTER, 3: BOTTOM.
	 */
	public static void showToastErro(Activity activity, int mensagem, int posicao)
	{
		ToastAlertaMensagem alertaMensagem = ToastAlertaMensagem.makeText(activity, activity.getString(mensagem), ToastAlertaMensagem.STYLE_ALERT);
    	alertaMensagem.setDuration(ToastAlertaMensagem.LENGTH_LONG);
    	
		switch (posicao) {
		case 1:
			alertaMensagem.setLayoutGravity(Gravity.TOP);
			break;
		case 2:
			alertaMensagem.setLayoutGravity(Gravity.CENTER);
			break;
		case 3:
			alertaMensagem.setLayoutGravity(Gravity.BOTTOM);
			break;
		default:
			break;
		}
    	
    	alertaMensagem.show();
	}
	
	
	/**
	 * @param activity
	 * @param mensagem
	 * @param 1: TOP, 2: CENTER, 3: BOTTOM.
	 */
	public static void showToastErro(Activity activity, String mensagem, int posicao)
	{
		ToastAlertaMensagem alertaMensagem = ToastAlertaMensagem.makeText(activity, mensagem, ToastAlertaMensagem.STYLE_ALERT);
    	alertaMensagem.setDuration(ToastAlertaMensagem.LENGTH_LONG);
    	
    	switch (posicao) {
		case 1:
			alertaMensagem.setLayoutGravity(Gravity.TOP);
			break;
		case 2:
			alertaMensagem.setLayoutGravity(Gravity.CENTER);
			break;
		case 3:
			alertaMensagem.setLayoutGravity(Gravity.BOTTOM);
			break;
		default:
			break;
		}
    	
    	alertaMensagem.show();
	}
}
