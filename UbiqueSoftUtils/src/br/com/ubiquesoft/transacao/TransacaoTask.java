package br.com.ubiquesoft.transacao;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;
import br.com.ubiquesoft.utils.AndroidUtils;
import br.com.ubiquesoft.utils.Orientacao;

/**
 * Classe para controlar a thread
 * 
 * @author Elson Costa
 * 
 */
public class TransacaoTask extends AsyncTask<Void, Void, Boolean> {
	private static final String TAG = "livroandroid";
	private final Activity activity;
	private final Transacao transacao;
	private ProgressDialog progresso;
	private Throwable exceptionErro;
	private int aguardeMsg;
	public TransacaoTask(Activity activity, Transacao transacao, int aguardeMsg) 
	{
		this.activity = activity;
		this.transacao = transacao;
		this.aguardeMsg = aguardeMsg;
	}
	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		// Inicia a janela de aguarde...
		abrirProgress();
		//travar orientação para o usuario não reiniciar, o donwload novamente.
        Orientacao.travaOrientacao(activity);
	}
	@Override
	protected Boolean doInBackground(Void... params) {
		try {
			transacao.executar();
		} catch (Throwable e) {
			Log.e(TAG, e.getMessage(), e);
			// Salva o erro e retorna false
			this.exceptionErro = e;
			return false;
		} finally {
			try {
				fecharProgress();
			} catch (Exception e) {
				Log.e(TAG, e.getMessage(), e);
			}
		}
		// OK
		return true;
	}
	@Override
	protected void onPostExecute(Boolean ok) {
		if (ok) {
			// Transa��o executou com sucesso
			transacao.atualizarView();
		} else {
			// Erro
			AndroidUtils.alertDialog(activity, "Erro: " + exceptionErro.getMessage());
		}
	}
	public void abrirProgress() 
	{		
		try
		{
			progresso = ProgressDialog.show(activity, "", activity.getString(aguardeMsg));
		} catch (Throwable e) 
		{
			Log.e(TAG, e.getMessage(), e);
			//destrava orientação para o usuario não reiniciar, o donwload novamente.
	        Orientacao.destravarOrientacao(activity);
		}
	}
	public void fecharProgress() 
	{
		try 
		{
			if (progresso != null) {
				progresso.dismiss();
			}
		} catch (Throwable e) 
		{
			Log.e(TAG, e.getMessage(), e);
		}
		
		//destrava orientação para o usuario não reiniciar, o donwload novamente.
        Orientacao.destravarOrientacao(activity);
	}
}
