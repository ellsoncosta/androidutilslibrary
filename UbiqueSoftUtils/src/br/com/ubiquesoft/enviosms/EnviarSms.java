package br.com.ubiquesoft.enviosms;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.telephony.SmsManager;
import br.com.ubiquesoft.alerta.ToastAlerta;
import br.com.ubiquesoft.utils.FormatarString;

public class EnviarSms 
{
	private Activity activity;
	private boolean retorno = true;
	public EnviarSms(Activity activity)
	{
		this.activity = activity;
	}
	
	public boolean Enviar(String numeroTelefone, String Mensagem) 
	{
		String EnviandoSms = "Enviado Menssage.";
		String SmsEntregue = "Mensagem Entregue.";
		try 
		{
			PendingIntent setPI = PendingIntent.getBroadcast(activity, 0, new Intent(EnviandoSms), 0);
			PendingIntent deliveredPI = PendingIntent.getBroadcast(activity, 0, new Intent(SmsEntregue), 0);

			//enviando mensagem
			activity.registerReceiver(new BroadcastReceiver() 
			{				
				public void onReceive(Context context, Intent intent) 
				{
					 switch (getResultCode()) 
					 {
					 	case Activity.RESULT_OK:
					 		//mensagemToast.Curta(context, "Mensagem enviada.");
					 		retorno = true;
						break;
						
					 	case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					 		ToastAlerta.showToastInfor(activity, "falha gen�rica.", 3);
					 		break;
					 		
					 	case SmsManager.RESULT_ERROR_NO_SERVICE:
					 		ToastAlerta.showToastInfor(activity, "Sem servico.", 3);
					 		break;
					 }
				}
			}, new IntentFilter(EnviandoSms));
			
			//Mensagem entregue
			activity.registerReceiver(new BroadcastReceiver() 
			{				
				public void onReceive(Context context, Intent intent) 
				{
					 switch (getResultCode()) 
					 {
					 	case Activity.RESULT_OK:
					 		//mensagemToast.Curta(context, "Mensagem enviada.");
					 		retorno = true;
						break;
						
					 	case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					 		ToastAlerta.showToastInfor(activity, "falha gen�rica.", 3);
					 		break;
					 		
					 	case SmsManager.RESULT_ERROR_NO_SERVICE:
					 		ToastAlerta.showToastInfor(activity, "Sem servico.", 3);
					 		break;
					 }
				}
			}, new IntentFilter(SmsEntregue));
			
			
			SmsManager sms = SmsManager.getDefault();
			
			if (FormatarString.VerificarCarateresEspeciaisSMS(Mensagem)) 
			{
				if (Mensagem.length() <= 70) 
				{
					sms.sendTextMessage(numeroTelefone, null, Mensagem, setPI, deliveredPI);
				}
				else
				{
					sms.sendMultipartTextMessage(numeroTelefone, null, sms.divideMessage(Mensagem), null, null);
				}
			}
			else
			{
				if (Mensagem.length() < 160) 
				{
					sms.sendTextMessage(numeroTelefone, null, Mensagem, setPI, deliveredPI);
				}
				else
				{
					sms.sendMultipartTextMessage(numeroTelefone, null, sms.divideMessage(Mensagem), null, null);
				}
			}
			
			
		}
		catch (Exception e) 
		{
			ToastAlerta.showToastAlert(activity, "Erro " + e.toString(), 3);
			return false;
		}
		
		return retorno;
	}
}
