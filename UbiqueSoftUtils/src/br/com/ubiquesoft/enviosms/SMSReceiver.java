package br.com.ubiquesoft.enviosms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import br.com.ubiquesoft.utils.mensagemToast;

public class SMSReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		
		//---get the SMS message passed in---
        Bundle bundle = intent.getExtras();        
        SmsMessage[] messages = null;
        String str = "";            
        if (bundle != null)
        {
            //---retrieve the SMS message received---
            Object[] pdus = (Object[]) bundle.get("pdus");
            messages = new SmsMessage[pdus.length];            
            for (int i=0; i<messages.length; i++){
                messages[i] = SmsMessage.createFromPdu((byte[])pdus[i]);                
                str += "Menssage de: " + messages[i].getOriginatingAddress();                     
                str += ".";
                str += "\n";
                str += "" + messages[i].getMessageBody().toString();
                str += "\n";        
            }
            //---display the new SMS message---	            
            mensagemToast.Longa(context, str);
            
            //send a broadcast intent do ujpdate the sms received in a txtview
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction("SMS_RECEIVED_ACTION");
            broadcastIntent.putExtra("sms", str);
            context.sendBroadcast(broadcastIntent);

        }
	}

}
